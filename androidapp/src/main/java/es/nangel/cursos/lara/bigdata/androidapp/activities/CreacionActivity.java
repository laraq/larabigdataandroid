package es.nangel.cursos.lara.bigdata.androidapp.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.android.volley.Request;
import es.nangel.cursos.lara.bigdata.MetaDataModel;
import es.nangel.cursos.lara.bigdata.Usuario;
import es.nangel.cursos.lara.bigdata.androidapp.R;
import es.nangel.cursos.lara.bigdata.androidapp.network.NET;
import es.nangel.cursos.lara.bigdata.androidapp.network.NetCommand;
import es.nangel.cursos.lara.bigdata.androidapp.network.RequestProcessor;
import es.nangel.cursos.lara.bigdata.utils.Sexo;

public class CreacionActivity extends ActionBarActivity {
    private EditText name;
    private EditText edad;
    private Spinner genero;
    private Button crearUsuario;
    RequestProcessor<Usuario> requestProcessor = new RequestProcessor<Usuario>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creacion);

        //El -1 es por defecto, si no existe el id, pone ese valor.
        int idUsuario = getIntent().getIntExtra("idUsuario",-1);
        int otroIDUsuario = getIntent().getExtras().getInt("otroID", -1);

        name = (EditText)findViewById(R.id.activity_creacion_edit_name);
        edad = (EditText)findViewById(R.id.activity_creacion_edit_edad);
        genero = (Spinner)findViewById(R.id.activity_creacion_spinner_genero);

        crearUsuario = (Button)findViewById(R.id.activity_creacion_button_create_user);
        crearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().isEmpty()){
                    name.setError("Mamonazo");
                }else{
                    Usuario usuario = new Usuario();
                    usuario.setName(name.getText().toString());
                    usuario.setEdad(Integer.valueOf(edad.getText().toString()));
                    usuario.setGenero(Sexo.valueOf(getResources().getStringArray(R.array.generos_keys)[(int)genero.getSelectedItemId()]));
                    MetaDataModel<Usuario> userMetadata = new MetaDataModel<Usuario>();
                    userMetadata.setModel(usuario);
                    userMetadata.setReg_id("blabla");
                    String usuarioJSON = RequestProcessor.encodeBodyToJSON(userMetadata);
                    requestProcessor.returnObject(NET.URL_USUARIOS_GET_ALL, Request.Method.PUT, CreacionActivity.this, new NetCommand<Usuario>() {
                        @Override
                        public void onSucceded(Usuario usuario) {

                        }

                        @Override
                        public void onFailure() {

                        }
                    },usuarioJSON);
                }
            }
        });
    }
}
