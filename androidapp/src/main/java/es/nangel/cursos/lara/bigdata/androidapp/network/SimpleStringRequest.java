package es.nangel.cursos.lara.bigdata.androidapp.network;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.nio.charset.Charset;

/**
 * Created by laraquijano on 9/8/15.
 */
public class SimpleStringRequest extends StringRequest{

    public SimpleStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    public String getPostBodyContentType() {
        return "application/json";
    }

    @Override
    protected String getParamsEncoding() {
        return "UTF-8";
    }

    @Override
    protected String getPostParamsEncoding() {
        return "UTF-8";
    }
}
