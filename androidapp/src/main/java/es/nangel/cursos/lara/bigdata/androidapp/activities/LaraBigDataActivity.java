package es.nangel.cursos.lara.bigdata.androidapp.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.Request;
import es.nangel.cursos.lara.bigdata.Usuario;
import es.nangel.cursos.lara.bigdata.androidapp.network.NET;
import es.nangel.cursos.lara.bigdata.androidapp.network.NetCommand;
import es.nangel.cursos.lara.bigdata.androidapp.R;
import es.nangel.cursos.lara.bigdata.androidapp.network.RequestProcessor;

import java.util.List;


public class LaraBigDataActivity extends ActionBarActivity {
    private TextView nombre;
    private Button button;
    private RequestProcessor<Usuario> usuarioProcessor = new RequestProcessor<Usuario>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lara_big_data);
        nombre = (TextView)findViewById(R.id.activity_lara_big_data_name);
        button = (Button)findViewById(R.id.activity_lara_big_data_button_crear_usuario);

        //changeUsuarioData();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LaraBigDataActivity.this, CreacionActivity.class);
                intent.putExtra("idUsuario", 3);
                Bundle bundle = new Bundle();
                bundle.putInt("otroID",4);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void changeUsuarioData(){
        usuarioProcessor.returnListObject(NET.URL_USUARIOS_GET_ALL, this, new NetCommand<List>() {
            @Override
            public void onSucceded(List usuarios) {
                Usuario primerUsuario = (Usuario) usuarios.get(0);
                nombre.setText(primerUsuario.getName());
                button.setText(String.valueOf(primerUsuario.getEdad()));
            }

            @Override
            public void onFailure() {
                nombre.setText("That didn't work!");
            }
        },"");
    }

}
