package es.nangel.cursos.lara.bigdata.androidapp.network;

import android.content.Context;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by laraquijano on 9/8/15.
 */
public class RequestProcessor<T>{
    /**
     * Copiado y pegado de internet para hacer peticiones a internet y haga unas acciones en la respuesta.
     */
    public void returnObject(final String url,final int method, final Context context, final NetCommand<T> netCommand, final String body){
        //
        RequestQueue queue = Volley.newRequestQueue(context);
        final Type listType = new TypeToken<T>() { }.getType();
        Response.Listener<String> succededListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Copiado y pegado tmb para parsear json a mi clase.
                Gson gson = new Gson();
                //Copiado y pegado tmb de internet para q funcione gson.

                //Copiado y pegado tmb para parsear json a mi clase.
                T element = gson.fromJson(response, listType);
                netCommand.onSucceded(element);
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                netCommand.onFailure();
            }
        };
        doExecuteRequest(queue, url, method,context,netCommand, body, succededListener,errorListener);
    }

    private void doExecuteRequest(final RequestQueue queue, final String url,final int method, final Context context, final NetCommand<T> netCommand, final String body, Response.Listener<String> succededListener, Response.ErrorListener errorListener){
        StringRequest stringRequest;
        if(body.isEmpty()){
            stringRequest = new SimpleStringRequest(method,url,succededListener,errorListener);
        }else{
            stringRequest = new BodyStringRequest(method,url,succededListener,errorListener,body);
        }
        executeRequest(queue, stringRequest);
    }

    private void executeRequest(final RequestQueue queue, final Request request){
        queue.add(request);
    }

    public void returnListObject(final String url, final Context context, final NetCommand<List> netCommand, final String body){
        RequestQueue queue = Volley.newRequestQueue(context);
        final Type listType = new TypeToken<ArrayList<T>>() { }.getType();
        final Gson gson = new Gson();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        List<T> lista = gson.fromJson(response, listType);
                        netCommand.onSucceded(lista);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                netCommand.onFailure();
            }
        });

        queue.add(stringRequest);
    }

    public final static <T> String encodeBodyToJSON(T t){
        Gson gson = new Gson();
        return gson.toJson(t);
    }


}
