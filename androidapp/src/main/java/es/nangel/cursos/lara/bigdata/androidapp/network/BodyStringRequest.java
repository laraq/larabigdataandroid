package es.nangel.cursos.lara.bigdata.androidapp.network;

import android.content.Context;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by laraquijano on 9/8/15.
 */
public class BodyStringRequest extends SimpleStringRequest{
    private String body;
    public BodyStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, String body) {
        super(method, url, listener, errorListener);
        this.body = body;
    }

    @Override
    public byte[] getBody() {
        return body.getBytes(Charset.forName("UTF-8"));
    }

    @Override
    public byte[] getPostBody() throws AuthFailureError {
        return body.getBytes(Charset.forName("UTF-8"));
    }
}
