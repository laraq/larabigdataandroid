package es.nangel.cursos.lara.bigdata.androidapp.network;

/**
 * Created by laraquijano on 9/8/15.
 */

/**
 * Patrón command
 */
public interface NetCommand<T> {
    void onSucceded(T t);
    void onFailure();
}
