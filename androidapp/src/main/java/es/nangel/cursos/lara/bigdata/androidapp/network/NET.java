package es.nangel.cursos.lara.bigdata.androidapp.network;

import java.net.URL;

/**
 * Created by laraquijano on 9/8/15.
 */
public class NET {
    private static final String URL_BASE = "http://192.168.1.101:8080";
    private static final String URL_USUARIOS = "/usuarios/";
    public static final String URL_USUARIOS_GET_ALL = URL_BASE+URL_USUARIOS;
}
